---
layout: post
title: "Proposition Les normes du numérique"
date: 2017-03-10 12:00
comments: true
published: true
description: ""
categories:
- carnet
---
# La contrainte et la création de normes dans les gestionnaires de contenus sur le web

Appel à contribution : [Les normes du numérique](http://calenda.org/398702).

Idée : 15 années de CMS à la Wordpress a eu des influences sur les modes d'écriture (WYSIWYG), les modes de publication et les formes (thèmes par défaut de Wordpress). L'arrivée d'une nouvelle génération — en l'occurrence les SSG ou static site generators —, ou d'un nouveau modèle de CMS — la JAMstack —, va-t-elle induire l'absence de normes imposées par l'outil, et de nouvelles normes désormais produites par les créateurs et non par les outils ?

Si la majorité des sites web administrables utilisent Wordpress, quelle peut être l'influence de cette "norme" par défaut ? Sommes-nous condamnés à voir


#

Comment le standard EPUB peut avoir une influence sur les modes d'écriture : forme, contraintes, etc. Par exemple le fait de ne pas pouvoir facilement ajouter des contenus multimédias. Différence norme et standard, si la norme n'est pas implémentée partout : caca.


## Titre
Wordpress comme forme d'une norme de publication numérique

## Disciplines
Design, sciences de l'information, architecture de l'information

## Concept-clés
Chaînes de publication, design des interfaces

## Résumé (200 mots maximum) :
Le logiciel Wordpress est encore aujourd'hui le système de gestion de contenus — ou CMS pour Content Management System en anglais — le plus utilisé pour la création et l'administration de sites web. Si un tel outil a permis une démocratisation indéniable du web tant du côté des utilisateurs amateurs que de certains professionnels du web, il a peut-être également engendré une certaine forme d'homogénéité voir de norme sur différents plans : modes de publication en ligne, design, structure des contenus, architecture des données, interactions, esthétique, etc. Il ne s'agit pas ici d'interroger uniquement l'influence de Wordpress, d'autres systèmes de gestion de contenu sont concernés, et cet article sera l'occasion d'explorer d'autres approches de la publication sur le web. Entre innovation et dette technique, démocratisation et hégémonie, pratiques amateurs et logiques capitalistes, logiciels et chaînes de publication, un CMS comme Wordpress est-il la raison, la matérialisation ou le résultat d'une forme de norme numérique ?


## Plan provisoire
1. D'HTML aux CMS : une histoire des outils et services de publication sur le web
2. L'avénement de Wordpress entre démocratisation et professionnalisation
3. L'influence des CMS sur les pratiques de publication
4. Les limites d'une hégémonie
5. Les nouvelles approches



## Adresse e-mail
antoine@quaternum.net

## Nom
Fauchié

## Prénom
Antoine

## Bio-bibliographie (150 mots maximum)
Spécialisé dans le document numérique, Antoine Fauchié travaille entre le web et le livre numérique. Responsable de l'Unité d'enseignement "Piloter un projet d'édition numérique" du Master 2 Publication numérique de l'École nationale supérieure des sciences de l'information et des bibliothèques, il est l'auteur de plusieurs articles ayant pour sujets le livre, la structuration de l'information ou les chaînes de publication :

- "Workflows, formats", numéro 3 de la revue *Après\Avant* des Rencontres internationales de Lure, mai 2015
- "Le fanzinat des gens du web", intervention lors de la conférence Sud Web 2016, retranscription disponible en ligne : https://www.quaternum.net/2016/06/02/le-fanzinat-des-gens-du-web/
- "Une chaîne de publication inspirée du web" sur mon carnet web personnel, mars 2017 : https://www.quaternum.net/2017/03/13/une-chaine-de-publication-inspiree-du-web/
- "Le livre web comme objet d’édition ?" dans *Design et innovation dans la chaîne du livre. Ecrire, éditer, lire à l'ère numérique*, ouvrage dirigé par Stéphane Vial et Marie-Julie Catoir-Brisson, Paris, PUF, avril 2017


## Titre
Markdown comme condition d'une norme de l'écriture numérique

## Disciplines
Sciences de l'information, informatique, architecture de l'information

## Concept-clés
Document structuré, langage sémantique, interopérabilité des données, littératie numérique.

## Résumé (200 mots maximum) :
Inventé par John Gruber au début des années 2000, Markdown est un langage sémantique qui permet d'écrire du HTML — Hyper Text Markup Language — avec un système de balisage bien plus léger. D'abord plébiscité par les développeurs pour rédiger leur documentation, cette syntaxe est désormais de plus en plus employée, notamment dans des applications numériques qui cherchent à se passer d'interfaces WYSIWYG — What You See Is What You Get : ce que l'on voit est ce que l'on obtient, fonctionnement des traitements de texte classiques. Pensé pour distinguer la structure et la mise en forme d'un document, et être très facilement transformable en HTML, Markdown devient le pivot de l'écriture numérique, rendant les fichiers sources tout autant lisibles par des humains, interopérables pour les machines ou résilients. En plus de Markdown, d'autres langages sémantiques comme Asciidoc semblent s'imposer face aux interfaces WYSIWYG qui n'ont pas résolu le problème de l'intéraction homme - texte structuré, et que LaTeX a limité à des usages universitaires. Si le standard du web est le HTML, comment Markdown peut-il être la norme de l'écriture numérique ?


## Plan provisoire
1. Le langage HTML comme standard de la lecture numérique
2. L'échec des traitements de texte et des interfaces WYSIWYG
3. La possibilité d'une norme via des langages sémantiques simples
4. Ce qui est permis par une telle norme


## Biographie - Auteurs de référence
Bachimont, Bruno et Crozat, Stéphane, "Instrumentation numérique des documents : pour une séparation fond/forme", Toulouse, Cépaduès-Éditions
Guichard, Éric, L’écriture scientifique: grandeur et misère des
technologies de l’intellect, Lyon, Sens-public (https://hal-ens.archives-ouvertes.fr/file/index/docid/347616/filename/CIPH2006.Guichard.pdf)
Pédauque, Roger T., Le document à la lumière du numérique, Caen, C&F Editions
Guichard
iA, Multichannel Text Processing, Zurich, iA (https://ia.net/topics/multichannel-text-processing/)
