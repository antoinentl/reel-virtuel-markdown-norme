## Titre
Markdown comme condition d'une norme de l'écriture numérique

## Disciplines
Sciences de l'information, informatique, architecture de l'information

## Concept-clés
Document structuré, langage sémantique, interopérabilité des données, littératie numérique.

## Résumé (200 mots maximum) :
Inventé par John Gruber au début des années 2000, Markdown est un langage sémantique qui permet d'écrire du HTML — Hyper Text Markup Language — avec un système de balisage bien plus léger. D'abord plébiscité par les développeurs pour rédiger leur documentation, cette syntaxe est désormais de plus en plus employée, notamment dans des applications numériques qui cherchent à se passer d'interfaces WYSIWYG — What You See Is What You Get : ce que l'on voit est ce que l'on obtient, fonctionnement des traitements de texte classiques. Pensé pour distinguer la structure et la mise en forme d'un document, et être très facilement transformable en HTML, Markdown devient le pivot de l'écriture numérique, rendant les fichiers sources tout autant lisibles par des humains, interopérables pour les machines ou résilients. En plus de Markdown, d'autres langages sémantiques comme Asciidoc semblent s'imposer face aux interfaces WYSIWYG qui n'ont pas résolu le problème de l'intéraction homme - texte structuré, et que LaTeX a limité à des usages universitaires. Si le standard du web est le HTML, comment Markdown peut-il être la norme de l'écriture numérique ?


## Plan provisoire
1. Le langage HTML comme standard de la lecture numérique
2. L'échec des traitements de texte et des interfaces WYSIWYG
3. La possibilité d'une norme via des langages sémantiques simples
4. Ce qui est permis par une telle norme


## Biographie - Auteurs de référence
Bachimont, Bruno et Crozat, Stéphane, "Instrumentation numérique des documents : pour une séparation fond/forme", Toulouse, Cépaduès-Éditions
Guichard, Éric, L’écriture scientifique: grandeur et misère des
technologies de l’intellect, Lyon, Sens-public (https://hal-ens.archives-ouvertes.fr/file/index/docid/347616/filename/CIPH2006.Guichard.pdf)
Pédauque, Roger T., Le document à la lumière du numérique, Caen, C&F Editions
Guichard
iA, Multichannel Text Processing, Zurich, iA (https://ia.net/topics/multichannel-text-processing/)
